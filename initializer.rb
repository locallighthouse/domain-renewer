APIKEY = ENV["NAMECHEAP_API_KEY"] # llh
APIUSER = ENV["NAMECHEAP_API_USER"]
# KM IP
# CLIENTIP = "216.14.37.242"
# RK IP
CLIENTIP = "192.155.80.208"
USERNAME = ENV["NAMECHEAP_USERNAME"]
URL = "https://api.namecheap.com/xml.response"
LLH_API_ENDPOINT = "http://api.llhdev.com/v1/projects_by_domain/get"
KEVIN_EMAIL = "kevin@locallighthouse.com"
SENDGRID_API_USER = ENV["SENDGRID_API_USER"]
SENDGRID_API_KEY  = ENV["SENDGRID_API_KEY"]

require 'sendgrid-ruby'
require 'httparty'
require 'uri/http'
require 'active_record'

ActiveRecord::Base.establish_connection(  
:adapter => "mysql2",  
:host => "localhost",  
:database => "renewer",
:username => "renewer",
:password => ENV["MYSQL_PASSWORD"]
)
