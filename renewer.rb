require_relative 'initializer.rb'
require_relative 'namecheap_api.rb'
require_relative 'sendgrid.rb'

class Renewer
  include NamecheapApi
  include HTTParty
  include SendGrid

  def initialize
    @update_log = File.open('update_log.txt', 'a')
    @renew_log = File.open('renew_log.txt', 'a')
    @expired_list_check_log = File.open('expired_list_check_log.txt', 'a')
  end

# GOD METHOD
  def all_the_things
    arg1 = clone_namecheap_api_to_db # output is array of 4 strings
    arg2 = renew_domains_subscriptions_and_wigs_protections
    arg3 = check_expired_list
    # update_client_status_and_lt_id_all_domains_records
    send_report(arg2)
    puts "#{arg1[0][:domains]}\n#{arg1[0][:wigs]}\n#{arg1[1]}\n#{arg1[2]}\n#{arg1[3]}\n#{arg2[0]}\n#{arg2[1]}\n#{arg3}"
    @update_log.puts "#{arg1[0][:domains]}\n#{arg1[0][:wigs]}\n#{arg1[1]}\n#{arg1[2]}\n#{arg1[3]}"
    @renew_log.puts "#{arg2[0]}\n#{arg2[1]}"
    @expired_list_check_log.puts "#{arg3}"
  end

# PACKAGE I.
  # Replicates BOTH domain and WIG data from API into DB
  def clone_namecheap_api_to_db
    msg_purge = delete_old_records_cancelled_and_expired_1_month_ago_or_more 
    msg_all = copy_namecheap_api_records_for_all_domains("ALL")
    msg_wigs = copy_namecheap_api_records_for_all_wigs
    output = [msg_purge,msg_all,msg_wigs]
  end

# PACKAGE II.
  # Renews expiring domains with wigs that are alloted to them
  def renew_domains_subscriptions_and_wigs_protections
    output_domain_renew_process = get_expiring_and_try_renew_all
    renewed_domain_names_array = output_domain_renew_process[:successes]
    msg_domains_renew = output_domain_renew_process[:msg_domain_renew]
    wigs_need_renewal   = renewed_domain_names_array.map{|dom_name| Wig.find_by(domain_name: dom_name)}
    msg_wigs_renew = renew_wigs_for_all_expiring(wigs_need_renewal) # output is msg_wigs_renew
    output = [msg_domains_renew, msg_wigs_renew]
  end

# PACKAGE III.
  # Gets "EXPIRED" list and double-checks each client_status as "cancelled"
  def check_expired_list
    msg_cancelled_verification = double_check_expired_are_cancelled
  end

# PACKAGE IV.
  # Assigns unused/free WhoisGuard subscriptions to domain names that need them.
  def exhaust_all_wig_subscriptions
    ids = Wig.where(status: "unused").pluck(:wig_id)
    domain_objects_array = Domain.where(who_is_guard: "NOTPRESENT").where.not(client_status: "cancelled").reject{|ca| ca.name.include?(".ca")}.reject{|us|us.name.include?(".us")}
    # .reject{|no_match| no_match.leadtrac_id == "No match found"}
    domain_name_array = domain_objects_array.map{|obj|obj.name}
    allot_and_enable_wig_multi(domain_name_array.reverse, ids)
  end

end
############################################
class Domain < ActiveRecord::Base

  def wig
    wig = Wig.find_by(domain_name: self.name)
    wig unless wig.nil?
  end

  def self.update_boolean_for_expired_domains
    expired = all.reject{|x|x.expires.nil?}.select{|x|Date.strptime(x.expires, "%m/%d/%Y") <= Date.today}
    expired.each do |exp|
      exp.update(:is_expired => true)
    end
  end

end
############################################
class Wig < ActiveRecord::Base

  def domain
    domain = Domain.find_by(name: self.domain_name)
    domain unless domain.nil?
  end

end
