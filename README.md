Domain Name Renewer
====================

###LLH clients with Namecheap deployments

### What is this project for?

#### Below is a description of the 4 packages this project includes:

### PACKAGES

I. Update of Namecheap API Domain and WhoisGuard(WIG) subscription records into Local Database

1. Removes any old records (cancelled and expired 1 month ago or before) from the database.  
2. Gets list of all LLH domains and WIGs that are active from the API.
3. Updates all existing database records with current information.
4. Creates new records in database for anything added to API list since last update.
5. Logs messages into "update_log.txt" file for every record created/updated, showing a running count for number of records processed and remaining.
6. Final log message gives total domain/WIG records count before and after this update package is run.

II. Renewal of Expiring Domain/WhoisGuard subscriptions for Current LLH Clients

1. Gets list of expiring domains (within the next 30 days) from API.
2. Individually, checks LLH status of clients that these domains belong to, and renews membership on only those that are not cancelled LLH clients.
3. On the same domain, finds the WIG allotted to it, and renews the WIG subscription.
4. Creates a log message into "renew_log.txt" detailing the renewal success, fail, or inaction for every domain in the expiring list and its corresponding WIG.

III. Assurance of All Cancelled Domains of Cancelled LLH Clients

1. Gets list of expired/cancelled domains (within the last 2 weeks) from API.
2. Individually, checks each domain for a "cancelled" status in LLH Leadtrac database.
3. Creates a log message into "expired_list_check_log.txt" with record details and client status feedback.
4. Final log message provides a count of total expired domains that passed and failed this check process.

IV. WhoIsGuard Subscription Allotment to Domains in Need

1. In one step, the IDs will be alloted to domains in need, and then enabled.
2. Gathers array of domain names that need WhoIsGuard subscriptions.
3. Gathers IDs for WhoIsGuard subscriptions that are unused and not allotted to any domains yet.

### To run in IRB ###

- Once in the appropriate directory, run: 

```
irb -r ./renewer.rb
```

- Create an instance of the class Renewer:

```
r = Renewer.new
```

- To run Packages I, II, and III (e.g. clone NC API, renew domains and wigs, and check expired), consecutively with one method: 

```
r.all_the_things
```

- To replicate BOTH domain and WIG data from API into DB (Package I): 

```
r.clone_namecheap_api_to_db
```

- To renew expiring domains and the wigs that are alloted to them (Package II): 

```
r.renew_domains_subscriptions_and_wigs_protections
```

- To get "EXPIRED" list from Namecheap's API and double-check each client status as "cancelled" (Package III):

```
r.check_expired_list
```

- To assign unused/free WhoisGuard subscriptions to domain names that need them, excluding domain ending in ".us" and ".ca" (Package IV): 

```
r.exhaust_all_wig_subscriptions
```


### Other useful methods:

- Replicates current domain data from API into database:
``` 
r.copy_namecheap_api_records_for_all_domains
```

- Replicates current WhoisGuard data from API into database:
```
r.copy_namecheap_api_records_for_all_wigs
```

- Renews domain name for all that are expiring, uncancelled clients:
```
r.get_expiring_and_try_renew_all
```

- Updates all domain records with current client status and Leadtrac ID:
```
r.update_client_status_and_lt_id_all_domains_records
```

- Creates or updates a domain record in database given an API domain object:
```
r.create_or_update_domain_record(domain_object)
```

- Creates or updates a WIG record in database given an API WIG object:
```
r.create_or_update_wig_record(wig_object)
```

- Gets the number of domains in NC API (includes domains that expired in the with last 2 weeks):
```
r.total_domains_count
```

- Gets the number of wigs in NC API (includes wigs that expired in the with last 2 weeks):
```
r.total_wigs_count
```

### Concerns (as of 4/21/15) ###
* Sandbox API will return "More than one match, 2" when executing #client_leadtrac_id(domain_name) with a domain name that has multiple LeadTrac numbers.  Current behavior is these domain names will be renewed.

* When a domain name expires (for a cancelled client), the WIG subscription allotted to is will get discarded, even though the subscription itself hasn't expired.  The only work around is to anticipate these domain names and unallot the subscription from it. Current behavior is we are letting subscriptions be discarded with their respective expired domains.

### Who do I talk to? ###

* Admin: Kevin McCabe
* Dev: Robert Kim