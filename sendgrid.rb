module SendGrid
  
  def sendgrid_client
    SendGrid::Client.new(api_user: SENDGRID_API_USER, api_key:SENDGRID_API_KEY)
  end
 
  def sendgrid_mail(arg2, to=KEVIN_EMAIL, bcc=SENDGRID_API_USER,from=SENDGRID_API_USER)
    adjusted_time = Time.current
    mail_hash = {
      to: to,
      bcc: bcc, 
      from: from, 
      subject: "LLH Domain & WhoisGuard Namecheap Renewal Report: #{adjusted_time.strftime("%-m/%-d/%Y")}", 
      html: "
      <div>
      -------------------------------------------------------------------------------------------------
      <br>
      LLH Domain Renewal Report<br>
      Date: #{adjusted_time.strftime("%-m/%-d/%Y")}<br>
      <br>
      <br>
      <br>

      <b>Local Lighthouse Management</b>,
      <br>
      <br>

      This report is to inform you of statistics for the domains renewal process.
      <br>
      <br>
      
      <b>Renewal of Domain Subscriptions and WIG protections</b>
      <br>
      <br>
      <b>1) Renewal of Domains (Expiring in the next 30 days)</b>
      <br>
      #{arg2[0]}
      <br>
      <br>
      <b>2) Renewal of WIG protections alotted to Expiring Domains</b>
      <br>
      #{arg2[1]}
      <br>
      <br>
      <br>
      
      Thanks,<br>
      Development Team
      <br>
      <br>

      <a href='http://www.locallighthouse.com/' title='LLH site'><img src='http://www.locallighthouse.com/images/lllogo.jpg' alt='www.locallighthouse.com' border='0' /></a><br />
      <br>
      --------------------------------------------------------------------------------------------------<br>

      </div>

      ", 
 
    }

    SendGrid::Mail.new(mail_hash)
  end

  def send_report(arg2)
    sendgrid_mail_res = sendgrid_mail(arg2)
    response = sendgrid_client.send(sendgrid_mail_res)
    puts "Response: #{response} | To: #{sendgrid_mail_res.to} | From: #{sendgrid_mail_res.from} | Subject: #{sendgrid_mail_res.subject}"
    { response: response, subject: sendgrid_mail_res.subject}

  end

  # # Sends notification email of pending domain expiration
  # def email_notification_domain_expires_soon(domain_name)
  #   res  = whois_lookup(domain_name).properties
  #   to          = res[:registrant_contacts].first.email
  #   name        = res[:registrant_contacts].first.name
  #   expiration  = res[:expires_on].strftime("%Y-%m-%d")

  #   send_report(to, name, domain_name, expiration)
  # end

end