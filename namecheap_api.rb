module NamecheapApi
  # I.A) Purges database of non-relevant domain records
  def delete_old_records_cancelled_and_expired_1_month_ago_or_more
    wigs_destroyed_counter = 0
    before_count  = [Domain.count, Wig.count]
    Domain.update_boolean_for_expired_domains
    stales = Domain.where(client_status: "cancelled").select{|x|Date.strptime(x.expires, "%m/%d/%Y") < 1.month.ago}
    stales_count = stales.count
    msg_tables_purge_start = "#{Time.current} | Domains/WIG tables purge BEGIN | Domain count BEFORE: #{before_count.first} | Wig count BEFORE: #{before_count.last}| Total domains to destroy: #{stales_count}"
    puts msg_tables_purge_start
    @update_log.puts msg_tables_purge_start
    stales.each_with_index do |stale,index|
      if stale.wig
        wig = stale.wig
        wig.destroy
        wigs_destroyed_counter += 1
        msg_stale_wig_deleted = "#{Time.current} | Stale WIG deleted | NC WIG ID: #{wig.wig_id} | LT ID: #{stale.leadtrac_id} | Wigs destroyed count: #{wigs_destroyed_counter} | #{wig.domain_name}"
        puts msg_stale_wig_deleted
        @update_log.puts msg_stale_wig_deleted
      else
        wig = Wig.where(status: "unused").last
      end
      leadtrac_id = leadtrac_id_get_or_add(stale.name)
      stale.destroy
      msg_stale_domain_deleted = "#{Time.current} | Stale Domain deleted | NC Domain ID: #{stale.domain_id} | LT ID: #{stale.leadtrac_id} | Remaining: #{stales_count - (index + 1)} | #{stale.name}"
      puts msg_stale_domain_deleted
      @update_log.puts msg_stale_domain_deleted
    end
    after_count = [Domain.count, Wig.count]
    msg_domains_purge_end = "#{Time.current.strftime("%m/%d/%Y, %I:%M %P")} | Expired Domains Purge Process | Domain count BEFORE: #{before_count.first} | AFTER: #{after_count.first} | Total domains destroyed: #{stales.count}"
    msg_wigs_purge_end = "#{Time.current.strftime("%m/%d/%Y, %I:%M %P")} | Expired WhoisGuard Purge Process | WIG count BEFORE: #{before_count.last} | AFTER: #{after_count.last} | Total WIGs Destroyed: #{wigs_destroyed_counter}"
    puts msg_domains_purge_end
    @update_log.puts msg_domains_purge_end
    puts msg_wigs_purge_end
    @update_log.puts msg_wigs_purge_end
    {domains: msg_domains_purge_end, wigs: msg_wigs_purge_end}
  end

  # I.B) Replicates current domain data from API into database
  def copy_namecheap_api_records_for_all_domains(list_type="ALL")
    before_count  = Domain.count
    counter       = 0
    objects       = get_domain_objects_for_all_pages(list_type)
    domains       = objects[:domains]
    api_count     = objects[:count]
    puts "#{Time.current} | #{list_type} Domains Copy START | Domain count BEFORE: #{before_count}"
    @update_log.puts "#{Time.current} | #{list_type} Domains Copy START | Domain count BEFORE: #{before_count}"
    domains.each do |domain|
      create_or_update_domain_record(domain)
      counter += 1
      puts "Domain copy count: #{counter} | Remaining: #{api_count - counter} of #{api_count}"
      @update_log.puts "Domain copy count: #{counter} | Remaining: #{api_count - counter} of #{api_count}"
    end
    after_count = Domain.count
    msg_domains_copy_end = "#{Time.current.strftime("%m/%d/%Y, %I:%M %P")} | #{list_type.capitalize} Domains Copy Process | Domain Count BEFORE: #{before_count} | AFTER: #{after_count}"
    puts msg_domains_copy_end
    @update_log.puts msg_domains_copy_end
    msg_domains_copy_end
  end

  # I.C) Replicates current WhoisGuard data from API into database
  def copy_namecheap_api_records_for_all_wigs(list_type="ALL")
    before_count  = Wig.count
    counter       = 0
    objects       = get_wig_objects_for_all_pages(list_type)
    wigs          = objects[:wigs]
    api_count     = objects[:count]
    puts "#{Time.current} | WIG Copy START | WIG count BEFORE: #{before_count}"
    @update_log.puts "#{Time.current} | WIG Copy START | WIG count BEFORE: #{before_count}"
    wigs.each do |subscription|
      create_or_update_wig_record(subscription)
      counter += 1
      puts "WIG copy count: #{counter} | Remaining: #{api_count - counter} of #{api_count}"
      @update_log.puts "WIG copy count: #{counter} | Remaining: #{api_count - counter} of #{api_count}"
    end
    after_count = Wig.count
    msg_wigs_copy_end ="#{Time.current.strftime("%m/%d/%Y, %I:%M %P")} | WIG Copy Process | WIG count BEFORE: #{before_count} | WIG count AFTER: #{after_count}"
    puts msg_wigs_copy_end
    @update_log.puts msg_wigs_copy_end
    msg_wigs_copy_end
  end

  # I.D) Updates client status and leadtrac ID from KM Sandbox API into database
  def update_client_status_and_lt_id_all_domains_records
    domains = Domain.all.reject{|rej|rej.client_status == "cancelled"}
    uncancelled_count = domains.count
    domains.each_with_index do |domain,index|
      begin
        domain_name = domain.name
        status = client_status_get_or_add(domain_name)
        lt_id  = leadtrac_id_get_or_add(domain_name)
        count_msg = "Client Status Update Attempt: #{index + 1} | Remaining: #{uncancelled_count - (index + 1)} of #{uncancelled_count}"
        puts count_msg
        @update_log.puts count_msg
        # sleep rand(1..3)
      rescue Errno::ECONNREFUSED => e
        puts "Error: #{e} | Domain: #{domain_name}"
        retry
      end
    end
  end

  def create_or_update_domain_record(domain_object)
    domain = domain_object
    dom = Domain.find_by(domain_id: domain["ID"])
    if dom.nil?
      dom = Domain.create(domain_id: domain["ID"], name: domain["Name"], created: domain["Created"], expires: domain["Expires"], is_expired: domain["IsExpired"], is_locked: domain["IsLocked"], auto_renew: domain["AutoRenew"], who_is_guard: domain["WhoisGuard"], created_at: Time.current, updated_at: Time.current)
    else 
      dom.update(expires: domain["Expires"], is_expired: domain["IsExpired"], is_locked: domain["IsLocked"], auto_renew: domain["AutoRenew"], who_is_guard: domain["WhoisGuard"], updated_at: Time.current)
    end
    puts "#{Time.current} | Domain created/updated | NC ID: #{dom.domain_id} | LT ID: #{dom.leadtrac_id} | Client status: #{dom.client_status} | #{dom.name}"
    @update_log.puts "#{Time.current} | Domain created/updated | NC ID: #{dom.domain_id} | LT ID: #{dom.leadtrac_id} | Client status: #{dom.client_status} | #{dom.name}"
  end

  def create_or_update_wig_record(wig_object)
    subscription = wig_object
    wig = Wig.find_by(wig_id: subscription["ID"])
    if wig.nil?
      wig = Wig.create(wig_id: subscription["ID"], created: subscription["Created"], domain_name:  subscription["DomainName"], expires: subscription["Expires"], status: subscription["Status"], created_at: Time.current, updated_at: Time.current)
      wig.update(domain_id: wig.domain.try(&:id))
    else 
      wig.update(domain_name:  subscription["DomainName"], expires: subscription["Expires"], status: subscription["Status"], domain_id: wig.domain.try(&:id), updated_at: Time.current)
    end
    puts "#{Time.current} | WIG created/updated | NC ID: #{wig.domain.try(&:domain_id)} | WIG DOMAIN ID: #{wig.domain_id} | WIG LT ID: #{wig.domain.try(&:leadtrac_id)} | #{wig.domain_name}"
    @update_log.puts "#{Time.current} | WIG created/updated | NC ID: #{wig.domain.try(&:domain_id)} | WIG DOMAIN ID: #{wig.domain_id} | WIG LT ID: #{wig.domain.try(&:leadtrac_id)} | #{wig.domain_name}"
  end

  # II. Renews domain name for all that are expiring, uncancelled clients
  def get_expiring_and_try_renew_all
    expiring = Domain.all.select{|x|Date.strptime(x.expires, "%m/%d/%Y") > Date.yesterday && Date.strptime(x.expires, "%m/%d/%Y") <= 1.month.from_now && x.is_expired == false}.reject{|rej| rej.client_status == "cancelled"}.map{|name|name.name}
    count_begin = expiring.count
    output_domain_renew_process = try_renew_multi(expiring) # output is a hash :successes, :msg_domain_renew

    puts "#{Time.current} - Domains renewals process completed!"
    @renew_log.puts "#{Time.current} - Domains renewals process completed!"
    output_domain_renew_process # output is hash => :successes and :msg_domain_renew
  end

  # III. Tests that all expired domains are from cancelled former LLH clients.
  def double_check_expired_are_cancelled
    expired_objs = get_domain_objects_for_all_pages("EXPIRED")[:domains] # Expired in last 30 days
    puts "#{Time.current} | Cancelled Domain Check START | Domain count: #{expired_objs.count}"
    @expired_list_check_log.puts "#{Time.current} | Cancelled Domain Check START | Domain count: #{expired_objs.count}"
    check_pass = []
    check_fail = []
    expired_objs.each_with_index do |exp_obj, index|
      exp_dom_name = exp_obj["Name"]
      client_status = client_status_get_or_add(exp_dom_name)
      leadtrac_id = leadtrac_id_get_or_add(exp_dom_name)
      check_result = (client_status == "cancelled")
      puts "#{Time.current} | NC ID: #{exp_obj["ID"]} | LT ID: #{leadtrac_id} | Expires: #{exp_obj["Expires"]} | Status: #{client_status} | Remaining: #{expired_objs.count - (index+1)} of #{expired_objs.count}| #{exp_dom_name}"
      @expired_list_check_log.puts "#{Time.current} | NC ID: #{exp_obj["ID"]} | LT ID: #{leadtrac_id} | Expires: #{exp_obj["Expires"]} | Status: #{client_status} | Remaining: #{expired_objs.count - (index+1)} of #{expired_objs.count}| #{exp_dom_name}"
      (check_result == true) ? (check_pass << check_result) : (check_fail << check_result)
    end
    msg_verification = "#{Time.current.strftime("%m/%d/%Y, %I:%M %P")} | Cancelled Domain Verification Process | Total Expired Domains: #{expired_objs.count} | Verified: #{check_pass.count} | Invalid: #{check_fail.count}"
    puts msg_verification
    @expired_list_check_log.puts msg_verification
    msg_verification
  end

  def get_domain_objects_for_all_pages(list_type)
    count = total_domains_count(list_type)
    total_domain_pages_needed_count = (count % 100 === 0) ? (count / 100) : ((count / 100) + 1)
    page_range_numbers_array = (1..total_domain_pages_needed_count).to_a
    result = km_retry("domain", list_type, page_range_numbers_array)
    domains = result.map{|x|x["ApiResponse"]["CommandResponse"]["DomainGetListResult"]["Domain"]}.flatten
    {domains: domains, count: count}
  end

  def get_wig_objects_for_all_pages(list_type)
    count = total_wigs_count(list_type)
    total_wig_pages_needed_count = (count % 100 === 0) ? (count / 100) : ((count / 100) + 1)
    page_range_numbers_array = (1..total_wig_pages_needed_count).to_a
    result = km_retry("wig", list_type, page_range_numbers_array)
    wigs = result.map{|x|x["ApiResponse"]["CommandResponse"]["WhoisguardGetListResult"]["Whoisguard"]}.flatten
    {wigs: wigs, count: count}
  end

  def total_domains_count(list_type="ALL")
    response = HTTParty.get(URL, { query: {"ApiUser"=>APIUSER, "ApiKey"=>APIKEY, "UserName"=>USERNAME, "ClientIp"=>CLIENTIP, "Page"=>1, "PageSize"=>100, "ListType" => list_type, "Command" => "namecheap.domains.getList"} })
    response["ApiResponse"]["CommandResponse"]["Paging"]["TotalItems"].to_i
  end

  def total_wigs_count(list_type="ALL")
    response = HTTParty.get(URL, { query: {"apiuser"=>APIUSER, "apikey"=>APIKEY, "username"=>USERNAME, "ClientIp"=>CLIENTIP, "Page"=>1, "PageSize"=>100, "ListType" => list_type, "Command" => "Namecheap.Whoisguard.getlist" } })
    response["ApiResponse"]["CommandResponse"]["Paging"]["TotalItems"].to_i
  end

  def client_status_get_or_add(domain_name)
    domain = Domain.find_by(name:domain_name)
    if domain && domain.client_status != "cancelled"
      status = client_status(domain_name)
      if domain.client_status != status
        domain.update(client_status: status)
        msg_domain_update = "#{Time.current} - Client status updated to \"#{status}\" for #{domain_name}."
        puts msg_domain_update
        @update_log.puts msg_domain_update
      else
        msg_domain_no_update = "#{Time.current} - Client status no change from \"#{status}\" for #{domain_name}."
        puts msg_domain_no_update
        @update_log.puts msg_domain_no_update
      end
    elsif domain
      status = domain.client_status
    else
      status = "N/A"
    end
    status
  end

  def leadtrac_id_get_or_add(domain_name)
    domain = Domain.find_by(name:domain_name)
    if domain && domain.leadtrac_id.nil?
      lt_id = client_leadtrac_id(domain_name)
      domain.update(leadtrac_id: lt_id)
      puts "#{Time.current} - Leadtrac ID updated to \"#{lt_id}\" for #{domain_name}."
      @update_log.puts "#{Time.current} - Leadtrac ID updated to \"#{lt_id}\" for #{domain_name}."
    elsif domain
      lt_id = domain.leadtrac_id
    else
      lt_id = "N/A"
    end
    lt_id
  end

  def try_renew_multi(domain_names_array)
    expiring_count = domain_names_array.count
    successes = []
    counter_pass = 0
    counter_fail = 0
    counter_cancel = Domain.where(client_status: "cancelled", is_expired: false).count
    domain_names_array.each_with_index do |domain_name, index|
      count_msg = "Domain Renewal Attempt: #{index + 1} | Remaining: #{expiring_count - (index + 1)} of #{expiring_count}"
      output = try_renew(domain_name) # This is a hash of :domain_name, :renew_response
      unless output.nil?
        if output[:renew_response] == "true" || output[:renew_response] == true
          successes << output[:domain_name] 
          counter_pass += 1
        elsif output[:renew_response] == "false" || output[:renew_response] == false
          counter_fail += 1
        else
          counter_cancel += 1
        end
      end
      puts count_msg
      @renew_log.puts count_msg
      # sleep rand(1..3)
    end
    msg_domain_renew = "#{Time.current.strftime("%m/%d/%Y, %I:%M %P")} | Domains Renewal Complete | Renewal Successes: #{counter_pass} | Renewal Failures: #{counter_fail} | Initial Expiring Active LLH Clients Count: #{expiring_count} | Final Expiring Active LLH Clients Count: #{expiring_count - counter_pass} | Number of cancelled clients expiring: #{counter_cancel} "
    puts msg_domain_renew
    @renew_log.puts msg_domain_renew
    {successes: successes, msg_domain_renew: msg_domain_renew} # output is a hash :successes, :msg_domain_renew
  end

  def renew_wigs_for_all_expiring(wigs_expiring)
    expiring_count = wigs_expiring.count
    counter_fail = 0
    counter_pass = 0
    wigs_expiring.each_with_index do |wig_expiring, index|
      count_msg = "WIG Renewal Attempt: #{index + 1} | Remaining: #{expiring_count - (index + 1)} of #{expiring_count}"
      wig_expiring_id = wig_expiring.wig_id
      renew_status = renew_one_wig_subscription(wig_expiring_id)
      puts count_msg
      @renew_log.puts count_msg
      renew_status == "true" ? counter_pass += 1 : counter_fail += 1
      # sleep rand(1..3)    
    end
    msg_wigs_renew = "#{Time.current.strftime("%m/%d/%Y, %I:%M %P")} | WIGs Renewal Complete | WIGs renewal PASSED: #{counter_pass} | WIGs renewal FAILED: #{counter_fail} | Initial Expiring WIGs Count: #{expiring_count} | Final Expiring WIGs Count: #{expiring_count - counter_pass}"

    puts "#{Time.current} - WIGs renewals process completed!"
    @renew_log.puts "#{Time.current} - WIGs renewals process completed!"
    puts msg_wigs_renew
    @renew_log.puts msg_wigs_renew
    msg_wigs_renew
  end

  def renew_one_wig_subscription(whoisguardid)
    wig     = Wig.find_by(wig_id: whoisguardid)
    wig_exp = wig.expires
   
    response = HTTParty.get(URL, { query: {"apiuser"=>APIUSER, "apikey"=>APIKEY, "username"=>USERNAME, "ClientIp"=>CLIENTIP, "Command"=>"namecheap.whoisguard.renew", "WhoisguardID" => whoisguardid, "Years"=>1, "PromotionCode"=> "WGSPECIAL"} })
    if response["ApiResponse"]["Errors"]
      msg_wig_renew_succes = "#{Time.current} - WIG ID: #{whoisguardid} | Expires: #{wig_exp} | Renewal: Failed | Reason: #{response["ApiResponse"]["Errors"]["Error"]["__content__"]} | Domain: #{wig.domain_name}"
      puts msg_wig_renew_succes
      @renew_log.puts msg_wig_renew_succes
      status = false
    else
      wig = Wig.find_by(wig_id: whoisguardid) #ACTIVE RECORD
      wig.update(expires: (Date.today + 1.year).strftime("%m/%d/%Y") )
      msg_wig_renew_succes = "#{Time.current} -  WIG ID: #{whoisguardid} | Expires: #{wig.expires} | Renewal: Success | Charged Amount: #{response["ApiResponse"]["CommandResponse"]["WhoisguardRenewResult"]["ChargedAmount"]} | Domain: #{wig.domain_name}"
      puts msg_wig_renew_succes
      @renew_log.puts msg_wig_renew_succes
      status = true
    end
    status
  end

  def try_renew(domain_name)
    status = client_status_get_or_add(domain_name)
    if status == "cancelled"
      msg_not_renewed = "#{Time.current} | ID: #{client_leadtrac_id(domain_name)} | Renewal: Not renewed | Status: cancelled | Reason: Client has cancelled. | Domain: #{domain_name}"
      puts msg_not_renewed
      @renew_log.puts msg_not_renewed
      output = {domain_name:domain_name, renew_response: "cancelled"}
    else
      output = renew_domain_name(domain_name) # This is a hash of :domain_name, :renew_response
    end
    output # output is a hash of :domain_name, :renew_response
  end

  def renew_domain_name(domain_name)
    res = HTTParty.get(URL, { query: {"ApiUser"=>APIUSER, "ApiKey"=>APIKEY, "UserName"=>USERNAME, "ClientIp"=>CLIENTIP, "Command"=>'namecheap.domains.renew', "DomainName"=>domain_name, "Years" => 1, "ListType"=>"ALL", "Page"=>1, "PageSize"=>100, "SortBy"=>"EXPIREDATE_DESC"} })
    
    if res && res.parsed_response["ApiResponse"]
      renew_response = res.parsed_response["ApiResponse"].try{|y|y["CommandResponse"]}.try{|z|z["DomainRenewResult"]}.try{|w|w["Renew"]}
      if renew_response && renew_response == "true"
        msg_success = "#{Time.current} | LT_ID: #{client_leadtrac_id(domain_name)} | Renewal: Success | Status: #{client_status(domain_name)} | Domain: #{domain_name}"
        puts msg_success
        @renew_log.puts msg_success
      # ACTIVE RECORD
        domain = Domain.find_by(name: domain_name)
        domain.update(expires: (Date.today + 1.year).strftime("%m/%d/%Y") )
      else
        msg_failed  = "#{Time.current} | LT_ID: #{client_leadtrac_id(domain_name)} | Renewal: Failed | Status: #{client_status(domain_name)} | Reason: #{res.parsed_response["ApiResponse"]["Errors"]["Error"]["__content__"]}  | Domain: #{domain_name}"
        puts msg_failed
        @renew_log.puts msg_failed
      end
    else
      renew_response = res.parsed_response.try{|x|x["ApiResponse"]}.try{|y|y["CommandResponse"]}.try{|z|z["DomainRenewResult"]}.try{|w|w["Renew"]}
    end

    {domain_name:domain_name, renew_response:renew_response}
  end

  def allot_and_enable_wig_multi(domain_names, ids)
    domain_names.each_with_index do |domain_name, index|
      if try_allot_and_enable(domain_name)
        attempt = allot_and_enable_wig(domain_name, ids[index])
      end
    end
  end    

  def try_allot_and_enable(domain_name)
    status = client_status_get_or_add(domain_name)
    msg_not_alloted = "#{Time.current} | Allot/Enable: Not renewed | LT ID: #{client_leadtrac_id(domain_name)} | Status: cancelled | Reason: Client has cancelled. | Domain: #{domain_name}"
    if status == "cancelled"
      puts msg_not_alloted
      @renew_log.puts msg_not_alloted
      output = false
    else
      output = true
    end
    output
  end

  def allot_and_enable_wig(domain_name, whoisguardid)
    response = HTTParty.get(URL, { query: {"apiuser"=>APIUSER, "apikey"=>APIKEY, "username"=>USERNAME, "ClientIp"=>CLIENTIP, "Command"=>"Namecheap.Whoisguard.allot", "DomainName" => domain_name, "whoisguardid" => whoisguardid, "EnableWG" => "True", "ForwardedToEmail" => KEVIN_EMAIL } })
    if response.parsed_response["ApiResponse"]["Errors"] != nil
      msg_failed  = "#{Time.current} | Allot/Enable: Failed | LT_ID: #{client_leadtrac_id(domain_name)} | WIG_ID: #{whoisguardid} | Status: #{client_status(domain_name)} | Reason: #{response.parsed_response["ApiResponse"]["Errors"]["Error"]["__content__"]} | Domain: #{domain_name} "
      puts msg_failed
      @renew_log.puts msg_failed
      false
    else
      msg_success = "#{Time.current} | Allot/Enable: Success | LT_ID: #{client_leadtrac_id(domain_name)} | WIG_ID: #{whoisguardid} | Status: #{client_status(domain_name)} | Domain: #{domain_name} "
      puts msg_success
      dom_inst = Domain.find_by(name: domain_name)
      dom_inst.update(who_is_guard: "ENABLED")
      wig = Wig.find_by(wig_id: whoisguardid)
      wig.update(domain_name: domain_name, status: "enabled")
      wig.update(domain_id: dom_inst.id)
      @renew_log.puts msg_success
      true
    end
  end

  def client_status(domain_name)
    statuses = {"cancelled" => "cancelled", "1"=>"admin", "ach declined" => "ach declined", "suspended" => "suspended", "active" => "active"}
    response = llh_api_project(domain_name)
    if response.nil?
      "missing"
    else
      statuses[response.try{|y|y["Primary_Status__c"]}.try(&:downcase)]
    end  
  end

  def client_leadtrac_id(domain_name)
    llh_api_project(domain_name).try{|x|x["LeadTrac_Id__c"]}
  end

  def llh_api_project(domain_name)
    llh_api_options = { body: {"domain"=> domain_name}}
    JSON.parse(HTTParty.post(LLH_API_ENDPOINT, llh_api_options)).try(:first)
  end

  def km_retry(table_name, list_type, page_numbers_array)
    attempt = 0
    result = []
    array = page_numbers_array
    while array.reject(&:nil?).any? do
      puts "KM #{table_name.capitalize} Attempt ##{attempt + 1} for pages array" 
      array.each_with_index do |page, index|
        begin
          page_retry = get_list_specific_page(table_name, page, list_type) unless page.nil?
          page_retry = nil if page.nil?
        rescue => e
          puts "ERROR #{e}"
          puts "Page # #{page} | Pages left: #{array.reject(&:nil?).count}"
          next
        end
        unless page_retry.nil? || page_retry["ApiResponse"]["CommandResponse"].nil? 
          puts "API #{table_name.capitalize} Mining: Page #{page} | Pages left: #{(array.reject(&:nil?).count)}"
          result << page_retry
          array[index] = nil
        end
        # sleep rand(1..3)
      end
      attempt += 1
    end
    # end
    result
  end

  def get_list_specific_page(table_name, page, list_type)
    if table_name == "domain"
      HTTParty.get(URL, { query: {"ApiUser"=>APIUSER, "ApiKey"=>APIKEY, "UserName"=>USERNAME, "ClientIp"=>CLIENTIP, "PageSize"=>100, "ListType" => list_type, "Command" => "namecheap.domains.getList", "Page" => page } })
    else
      HTTParty.get(URL, { query: {"apiuser"=>APIUSER, "apikey"=>APIKEY, "username"=>USERNAME, "ClientIp"=>CLIENTIP, "Page" => page, "PageSize"=>100, "ListType" => list_type, "Command" => "Namecheap.Whoisguard.getlist"} })
    end
  end

end
